function register() {

  const usuarioCollab = {
    nome: document.getElementById('nome').value,
    email: document.getElementById('email').value,
    cpf: document.getElementById('cpf').value,
    senha: document.getElementById('senha').value,
    servicos: document.getElementById('servicos').value,
    estado: document.getElementById('estado').value,
    cidade: document.getElementById('cidade').value,
    cep: document.getElementById('cep').value,
    rua: document.getElementById('rua').value,
    complemento: document.getElementById('complemento').value,
    bairro: document.getElementById('bairro').value,
    telefone: document.getElementById('telefone').value,
  };
  let usuarios = existsUsers();

  usuarios.push(usuarioCollab);

  localStorage.setItem('usuariosCollab', JSON.stringify(usuarios));
  console.log(localStorage.getItem('usuariosCollab'));
  window.location.href = '../../homepets-grupo-04-homolog/index.html';
  alert("Sucesso!");
}

function existsUsers() {
  let usuarios = JSON.parse(localStorage.getItem('usuariosCollab'));

  if (!usuarios) {
    usuarios = [];
  }

  return usuarios;
}
