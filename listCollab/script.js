function voltar() {
  window.location.href = '../homeUser/index.html';
}

window.onload = function() {
  let tabela = document.getElementById("tabela");
  let tbody = tabela.getElementsByTagName("tbody")[0];

  let usuariosCollab = JSON.parse(localStorage.getItem('usuariosCollab'));
  let usuariosPj = JSON.parse(localStorage.getItem('usuariosPj'));

  if (usuariosCollab != null) {
    usuariosCollab.forEach(function(usuarioCollab, i) {
      tbody.innerHTML += "<tr><td>"+usuarioCollab.nome+"</td><td>"+usuarioCollab.email+"</td><td>"+usuarioCollab.telefone+"</td><td>"+usuarioCollab.servicos+"</td><td>"+usuarioCollab.cidade+"</td></tr>";      
    });
  }
 
  if (usuariosPj != null) {
    usuariosPj.forEach(function(usuarioPj, i) {
      tbody.innerHTML += "<tr><td>"+usuarioPj.nome+"</td><td>"+usuarioPj.email+"</td><td>"+usuarioPj.telefone+"</td><td>"+usuarioPj.servicos+"</td><td>"+usuarioPj.cidade+"</td><td>"+usuarioPj.tipoEmpresa+"</td><td>"+usuarioPj.nomeEmpresa+"</td></tr>";      
    });
  } 
};
