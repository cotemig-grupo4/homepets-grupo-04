function registerUser() {
  window.location.href = 'registerUser/index.html';
}

function registerCollaborator() {
  window.location.href = 'registerCollaborator/index.html';
}

function registerPj() {
  window.location.href = 'registerPj/index.html';
}

function login() {
  const values = {
    usuario: document.getElementById('usuario').value,
    senha: document.getElementById('senha').value,
  };

  let usuarios = JSON.parse(localStorage.getItem('usuarios'));
  let usuariosCollab = JSON.parse(localStorage.getItem('usuariosCollab'));
  let usuariosPj = JSON.parse(localStorage.getItem('usuariosPj'));

  if (usuarios != null) {
    usuarios.forEach(function(usuario, i) {
      if (usuario.email === values.usuario && usuario.senha === values.senha) {
        localStorage.setItem('login', JSON.stringify(login));
        window.location.href = 'homeUser/index.html';
        alert("Sucesso!");
      }
    })
  } else if (usuariosCollab != null) {
    usuariosCollab.forEach(function(usuarioCollab, i) {
      if (usuarioCollab.email === values.usuario && usuarioCollab.senha === values.senha) {
        localStorage.setItem('login', JSON.stringify(login));
        window.location.href = 'homeCollab/index.html';
        alert("Sucesso!");
      }
    })
  } else if (usuariosPj != null) {
    usuariosPj.forEach(function(usuarioPj, i) {
      if (usuarioPj.email === values.usuario && usuarioPj.senha === values.senha) {
        localStorage.setItem('login', JSON.stringify(login));
        window.location.href = 'homeCollab/index.html';
        alert("Sucesso!");
      }
    })
  } else {
    alert("Usuário ou senha incorretos!");
  }
}
