function register() {

  const usuarioPj = {
    nome: document.getElementById('nome').value,
    email: document.getElementById('email').value,
    cpf: document.getElementById('cpf').value,
    senha: document.getElementById('senha').value,
    nomeEmpresa: document.getElementById('nomeEmpresa').value,
    cnpj: document.getElementById('cnpj').value,
    servicos: document.getElementById('servicos').value,
    tipoEmpresa: document.getElementById('tipoEmpresa').value,
    estado: document.getElementById('estado').value,
    cidade: document.getElementById('cidade').value,
    cep: document.getElementById('cep').value,
    rua: document.getElementById('rua').value,
    complemento: document.getElementById('complemento').value,
    bairro: document.getElementById('bairro').value,
    telefone: document.getElementById('telefone').value,
  };
  let usuarios = existsUsers();

  usuarios.push(usuarioPj);

  localStorage.setItem('usuariosPj', JSON.stringify(usuarios));
  console.log(localStorage.getItem('usuariosPj'));
  window.location.href = '../../homepets-grupo-04-homolog/index.html';
  alert("Sucesso!");
}

function existsUsers() {
  let usuarios = JSON.parse(localStorage.getItem('usuariosPj'));

  if (!usuarios) {
    usuarios = [];
  }

  return usuarios;
}
